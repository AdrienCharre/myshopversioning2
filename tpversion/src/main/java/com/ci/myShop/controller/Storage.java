package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.Map;

import com.ci.myShop.model.Item;

public class Storage {
	Map<String,Item> itemMap = new HashMap();
	
	
	public void addItem(Item it) {
		itemMap.put(it.getName(), it);
	}
	
	public Item getItem(String label){
		return itemMap.get(label);
	}
	
	


}
