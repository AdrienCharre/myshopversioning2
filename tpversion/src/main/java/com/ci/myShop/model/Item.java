package com.ci.myShop.model;


import java.util.Scanner;

/**
 * @author soheibdehbi
 *
 */

public class Item {

	
	private String name;
	private int id;
	private Float price;
	private int nbrElt;
	
	Scanner scanner = new Scanner(System.in);

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public int getNbrElt() {
		return nbrElt;
	}

	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}

	
	String display() {
		
		System.out.println("Entrez votre nom");
		name = scanner.next();
		return name;
		
	}
	
	

}
